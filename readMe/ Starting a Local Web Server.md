# Starting a Local Web Server with Python

This guide will show you how to start a local web server using Python and access it through your web browser.

## Starting the Web Server

To start a local web server using Python, follow these steps:

1. Open your command line or terminal.

2. Navigate to the directory where your files are located.

3. Run the following command, replacing `<Port Number>` with the port number you want to use:

    ```bash
    python -m http.server <Port Number>
    ```

   This command starts a simple HTTP server using Python's built-in `http.server` module.

## Accessing the Local Web Server

Once the server is running, you can access it through your web browser:

1. Open your web browser.

2. Navigate to the following URL:

    http://localhost:<Port Number>
  

   Replace `<Port Number>` with the port number you specified when starting the server.

3. You should see a directory listing of the files in the directory where you started the server. Click on any file to view it in your browser.

## Example

For example, if you started the server on port 8000, you would access it using the following URL:

http://localhost:8000

