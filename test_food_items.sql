-- Insert test restaurants
INSERT INTO restaurants (name, address, phone) VALUES
('Pizza Place', '123 Main St', '555-1234'),
('Burger Joint', '456 Elm St', '555-5678');



-- Insert test food items
INSERT INTO food_items (restaurant_id, name, description, price) VALUES
(1, 'Pizza', 'Delicious cheese pizza', 9.99),
(2, 'Burger', 'Juicy beef burger', 5.99);


