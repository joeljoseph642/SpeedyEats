#!/bin/bash

echo" Processing test Order"

user_id=1
email="testuser@example.com"
total_amount=25.50
items='[{"name": "Pizza", "quantity": 2, "price": 10.00}, {"name": "Salad", "quantity": 1, "price": 5.50}]'

# Simulate Order Creation (Directly Insert into Database)
order_id=$(mysql -u test -p test Test -e "INSERT INTO orders (user_id, email, total_amount, items) VALUES ('$user_id', '$email', '$total_amount', '$items'); SELECT LAST_INSERT_ID();")

# Check if Order ID was Generated
if [[ -n "$order_id" ]]; then
    echo "Order Placed Successfully (Order ID: $order_id)"
else
    echo "Order Failed - Unable to create order in database"
    exit 1  # Exit with an error code
fi
# Database Verification (Ensure order was inserted correctly)
db_result=$(mysql -u test -p test Test -e "SELECT * FROM orders WHERE order_id=$order_id AND user_id=$user_id AND total_amount=$total_amount;")

if [[ -n "$db_result" ]]; then  # Check if result is not empty
    echo "Database Entry Confirmed"
else
    echo "Order Failed - No Database Entry"
    exit 1
fi
