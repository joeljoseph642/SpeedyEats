INSERT INTO users (first_name, last_name, email, password, phone) VALUES
('John', 'Doe', 'john@example.com', 'password', '555-1234'),
('Jane', 'Doe', 'jane@example.com', 'password', '555-5678');

INSERT INTO restaurants (name, address, phone) VALUES
('Pizza Place', '123 Main St', '555-1234'),
('Burger Joint', '456 Elm St', '555-5678');

INSERT INTO food_items (restaurant_id, name, description, price) VALUES
(1, 'Pizza', 'Delicious cheese pizza', 9.99),
(2, 'Burger', 'Juicy beef burger', 5.99);

INSERT INTO orders (user_id, restaurant_id, food_item_id, quantity, price) VALUES
(1, 1, 1, 2, 19.98), -- John orders 2 Pizzas
(2, 2, 2, 1, 5.99); -- Jane orders 1 Burger

-- Insert additional test orders
INSERT INTO orders (user_id, restaurant_id, food_item_id, quantity, price) VALUES
(1, 2, 2, 3, 17.97), -- John orders 3 Burgers
(2, 1, 1, 1, 9.99); -- Jane orders 1 Pizza

-- Update an existing order (change quantity and price)
UPDATE orders SET quantity = 4, price = 39.96 WHERE id = 1; -- John changes order to 4 Pizzas

