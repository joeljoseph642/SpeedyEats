-- Insert test restaurants
INSERT INTO restaurants (name, address, phone) VALUES
('Pizza Place', '123 Main St', '555-1234'),
('Burger Joint', '456 Elm St', '555-5678');
