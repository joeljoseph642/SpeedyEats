const textInputs = document.querySelectorAll("input[type='text']:not(.searchArea),input[type='password'], .stateTA,.postcodeTA,.suburbTA");
const icons = document.querySelectorAll(".userIcon1, .emailIcon, .phoneIcon,.lockIcon");

var placeholderOg = " ";
for (let i = 0; i < textInputs.length; i++) {
  textInputs[i].addEventListener("click", function () {
    this.style.backgroundColor = "rgb(241, 251, 220)";
    placeholderOg = this.placeholder;
    this.placeholder = " ";
    this.style.color = "black";
    icons[i].style.color = "black";
  });
}

for (let i = 0; i < textInputs.length; i++) {
  textInputs[i].addEventListener("blur", function () {
    this.style.backgroundColor = " rgba(73, 103, 54, 0.5)";
    this.placeholder = placeholderOg;
    this.style.color = "white";
    icons[i].style.color = "white";
  });
}



document.addEventListener('DOMContentLoaded', () => {
  const cartDetails = document.getElementById('cartDetails');

  const updateTotalPrice = () => {
    let total = 0;
    const items = cartDetails.querySelectorAll('.cartItem');
    items.forEach(item => {
      const price = parseFloat(item.querySelector('.priceVC').textContent.replace('$', ''));
      const quantity = parseInt(item.querySelector('.numberQ').textContent);
      total += price * quantity;
    });
    document.querySelector('.totalPr span:last-child').textContent = `$${total.toFixed(2)}`;
  };

  cartDetails.addEventListener('click', (event) => {
    if (event.target.closest('.increment')) {
      const numQ = event.target.closest('.qContainer').querySelector('.numberQ');
      let currentNumber = parseInt(numQ.textContent);
      numQ.textContent = currentNumber + 1;
      updateTotalPrice();
    }

    if (event.target.closest('.trash1')) {
      const numQ = event.target.closest('.qContainer').querySelector('.numberQ');
      let currentNumber = parseInt(numQ.textContent);
      if (currentNumber > 0) {
        numQ.textContent = currentNumber - 1;
      }
      updateTotalPrice();
    }
  });

  updateTotalPrice();
});

document.addEventListener('DOMContentLoaded', () => {
  const progressPoints = document.querySelectorAll('.progressPoint');
  const progressBar = document.querySelector('.progress');

  progressPoints.forEach(point => {
    point.addEventListener('click', (e) => {
      progressPoints.forEach(pt => pt.classList.remove('active'));
      e.target.classList.add('active');
      let activePointIndex = Array.from(progressPoints).indexOf(e.target);
      let widthPercentage = (activePointIndex + 1) / progressPoints.length * 100;
      progressBar.style.width = widthPercentage + '%';
    });
  });
});
document.addEventListener("DOMContentLoaded", function () {
  const p1 = document.getElementById("p1");
  p1.click();
  p1.style.background = '#ED961A';
  const progressPoints = document.querySelectorAll(".progressPoint:not(#p5)");  // Exclude #p5

  progressPoints.forEach(point => {
    point.addEventListener("click", function () {
      if (point.classList.contains("active")) {
        point.style.background = "#ED961A"; 
      } else {
        point.style.background = "#1B1B1B"; 
      }
      point.classList.toggle("active");
    });
  });
});

document.addEventListener("DOMContentLoaded", function () {
  const p1 = document.getElementById("p1");
  const osText = document.querySelector(".osText");
  osText.textContent = "Your order is being made now...";

  const progressPoints = document.querySelectorAll(".progressPoint:not(#p5)");

  progressPoints.forEach((point, index) => {
    point.addEventListener("click", function () {
      switch (index) {
        case 1:
          osText.textContent = "Your order is getting picked up...";
          break;
        case 2:
          osText.textContent = "Your order is coming to you...";
          break;
        case 3:
          osText.textContent = "Your order is coming to you...";
          break;
      }
    });
  });
});


document.addEventListener('DOMContentLoaded', () => {
  const delFee = document.getElementById('delFee');
  const overlayDelFee = document.querySelector('.overlayDelFee');
  const priceLay = document.querySelector('.priceLay');
  const ratingLay = document.querySelector('.ratingLay');
  const sortByLay = document.querySelector('.sortByLay');
  const overlayBackground = document.querySelector('.overlay-background');
  const Under30Min = document.getElementById('Under30Min');
  const apply1 = document.getElementById('apply1');
  const apply2 = document.getElementById('apply2');
  const apply3 = document.getElementById('apply3');
  const apply4 = document.getElementById('apply4');
  const rating = document.getElementById('rating');
  const price = document.getElementById('price');
  const sortBy = document.getElementById('sortBy');
  const reset1 = document.getElementById('reset1');
  const reset2 = document.getElementById('reset2');
  const reset3 = document.getElementById('reset3');
  const reset4 = document.getElementById('reset4');
  const options1 = document.querySelectorAll('.overlayDelFee .optionX');
  const options2 = document.querySelectorAll('.ratingLay .optionX');
  const options3 = document.querySelectorAll('.priceLay .optionX');
  const delText = document.getElementById('delText');
  const ratingText = document.getElementById('ratingText');
  const priceText = document.getElementById('priceText');


  const hideOverlayDel = () => {
    delFee.style.border = 'solid rgba(237, 150, 26, 1)';
    overlayDelFee.style.display = 'none';
    overlayBackground.style.display = 'none';
  };

  const hideRatingLay = () => {
    rating.style.border = 'solid rgba(237, 150, 26, 1)';
    ratingLay.style.display = 'none';
    overlayBackground.style.display = 'none';
  };

  const hidePriceLay = () => {
    price.style.border = 'solid rgba(237, 150, 26, 1)';
    priceLay.style.display = 'none';
    overlayBackground.style.display = 'none';
  };

  const hideSortByLay = () => {
    sortBy.style.border = 'solid rgba(237, 150, 26, 1)';
    sortByLay.style.display = 'none';
    overlayBackground.style.display = 'none';
  };

  apply1.addEventListener('click', () => {
    hideOverlayDel();
  });

  apply2.addEventListener('click', () => {
    hideRatingLay();
  });

  apply3.addEventListener('click', () => {
    hidePriceLay();
  });
  apply4.addEventListener('click', () => {
    hideSortByLay();
  });

  let under30MinClicked = false;

  const under30Min = document.getElementById('Under30Min');

  under30Min.addEventListener('click', () => {
    if (!under30MinClicked) {
      under30Min.style.background = "rgba(239, 251, 219, 1)";
      under30Min.querySelector('p').style.color = "black";
      under30MinClicked = true;
    } else {
      under30Min.style.background = "rgba(57, 57, 57, 1)";
      under30Min.querySelector('p').style.color = "white";
      under30MinClicked = false;
    }
  });
  delFee.addEventListener('click', () => {
    overlayDelFee.style.display = 'flex';
    overlayBackground.style.display = 'block';
  });

  rating.addEventListener('click', () => {
    ratingLay.style.display = 'block';
    overlayBackground.style.display = 'block';
  });

  price.addEventListener('click', () => {
    priceLay.style.display = 'block';
    overlayBackground.style.display = 'block';
  });
  sortBy.addEventListener('click', () => {
    sortByLay.style.display = 'block';
    overlayBackground.style.display = 'block';
  });

  document.addEventListener('click', (event) => {
    if (!overlayDelFee.contains(event.target) && !delFee.contains(event.target) &&
      !ratingLay.contains(event.target) && !rating.contains(event.target) &&
      !priceLay.contains(event.target) && !price.contains(event.target)) {
      hideOverlay();
    }
  });

  overlayDelFee.addEventListener('click', (event) => {
    event.stopPropagation();
  });

  ratingLay.addEventListener('click', (event) => {
    event.stopPropagation();
  });

  priceLay.addEventListener('click', (event) => {
    event.stopPropagation();
  });

  const resetDeliveryFee = () => {
    delText.textContent = '';
    options1.forEach(option => option.classList.remove('selected'));
  };

  const resetRating = () => {
    ratingText.textContent = '';
    options2.forEach(option => option.classList.remove('selected'));
  };

  const resetPrice = () => {
    priceText.textContent = '';
    options3.forEach(option => option.classList.remove('selected'));
  };

  reset1.addEventListener('click', () => {
    resetDeliveryFee();
  });

  reset2.addEventListener('click', () => {
    resetRating();
  });

  reset3.addEventListener('click', () => {
    resetPrice();
  });

  const updateDeliveryFee = (selectedFee) => {
    if (selectedFee === '$5+') {
      delText.textContent = 'Above 5+';
    } else {
      delText.textContent = `Under ${selectedFee}`;
    }
  };

  const updateRating = (selectedRating) => {
    if (selectedRating === '5') {
      ratingText.textContent = 'Only 5 stars';
    } else {
      ratingText.textContent = `Under ${selectedRating}`;
    }
  };

  const updatePrice = (selectedPrice) => {

  };

  options1.forEach(option => {
    option.addEventListener('click', () => {
      updateDeliveryFee(option.textContent.trim());
      options1.forEach(opt => opt.classList.remove('selected'));
      option.classList.add('selected');
    });

    option.addEventListener('blur', () => {
      option.classList.remove('selected');
    });
  });
  options2.forEach(option => {
    option.addEventListener('click', () => {
      updateRating(option.textContent.trim());
      options2.forEach(opt => opt.classList.remove('selected'));
      option.classList.add('selected');
    });

    option.addEventListener('blur', () => {
      option.classList.remove('selected');
    });
  });
  options3.forEach(option => {
    option.addEventListener('click', () => {
      updatePrice(option.textContent.trim());
      options3.forEach(opt => opt.classList.remove('selected'));
      option.classList.add('selected');
    });

    option.addEventListener('blur', () => {
      option.classList.remove('selected');
    });
  });
  reset4.addEventListener('click', function () {
    var radios = document.querySelectorAll('.radioB');
    radios.forEach(function (radio) {
      radio.checked = false;
    });
  });
});